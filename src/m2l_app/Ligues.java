/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m2l_app;

/**
 *
 * @author tphim
 */
public class Ligues {
    private int id;
    private String nom;
    private String activite;
    
    public Ligues(int id, String nom, String activite){
        this.id= id;
        this.nom=nom;
        this.activite=activite;
    }
    
    public int getId(){
        return id;
    }
    
    public String getNom(){
        return nom;
    }
    
    public String getActivite(){
        return activite;
    }
    
}
