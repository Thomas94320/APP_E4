/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m2l_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author johan
 */
public class DAO {

    private Connection co = null;
    public DAO() {
        
    }
    
    
    public void getConnection(){
        if(co == null) { 
        try
        {
            //loading the jdbc driver
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            //get a connection to database
            //Connection myConn=DriverManager.getConnection("jdbc:mysql://localhost:3306/e4_app","root","");
            co=DriverManager.getConnection("jdbc:mysql://localhost:3306/e4_sql","root","");
        }
         catch(Exception e){
             e.printStackTrace();
        }
        }
    }
    
    public DefaultListModel getLigues(){
        this.getConnection();
        DefaultListModel model= new DefaultListModel();
        try{
           Statement stmt1=co.createStatement();
           ResultSet rs=stmt1.executeQuery("select * from ligue");
           while (rs.next()){
               model.addElement(rs.getString("ligue_nom"));
           } 
        }catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }
    
    public DefaultListModel getClubs(){
        this.getConnection();
        DefaultListModel model= new DefaultListModel();
        try{
           Statement stmt1=co.createStatement();
           ResultSet rs=stmt1.executeQuery("select * from club");
           while (rs.next()){
               model.addElement(rs.getString("club_nom"));
           } 
        }catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }
    
    public DefaultListModel getLocations(){
        this.getConnection();
        DefaultListModel model= new DefaultListModel();
        try{
           Statement stmt1=co.createStatement();
           ResultSet rs=stmt1.executeQuery("select * from louer");
           while (rs.next()){
               model.addElement(rs.getString("louer_intitule"));
           } 
        }catch(Exception e){
            e.printStackTrace();
        }
        return model;
    }
    
    public String[] getDetails(String location){
        this.getConnection();
        String req = "(SELECT s.salle_nom,ligue.ligue_nom,l.`louer_date`,l.`louer_duree`,l.`louer_dateFin`,s.salle_batiment,s.salle_etage \n" +
"FROM louer l, entite e, ligue, salle s \n" +
"WHERE s.id_salle = l.id_salle AND l.louer_intitule= '"+location+"' AND l.id_entite = e.id_entite AND e.id_ligue = ligue.id_ligue AND e.id_club IS NULL)\n" +
"UNION\n" +
"(SELECT s.salle_nom,club.club_nom,l.`louer_date`,l.`louer_duree`,l.`louer_dateFin`,s.salle_batiment,s.salle_etage \n" +
"FROM louer l, entite e, club, salle s \n" +
"WHERE  s.id_salle = l.id_salle AND l.louer_intitule= '"+location+"' AND l.id_entite = e.id_entite AND e.id_club = club.id_club AND e.id_ligue IS NULL)";
        //DefaultListModel model= new DefaultListModel();
        String[] array = new String[7];
        try{
           Statement stmt1=co.createStatement();
           ResultSet rs=stmt1.executeQuery(req);
           while (rs.next()){
               for (int i=1;i<=7;i++){
                array[i-1]=rs.getString(i);
                }     
           } 
        }catch(Exception e){
            e.printStackTrace();
        }
        return array;
    }
    
    public DefaultListModel seeClubs(String ligue){
        this.getConnection();
        DefaultListModel model= new DefaultListModel();
        try{
                Statement stmt1=co.createStatement();
                Statement stmt2=co.createStatement();
                ResultSet rl=stmt1.executeQuery("select id_ligue from ligue where ligue_nom='"+ligue+"'");
                int id_ligue=0;
                while(rl.next()){
                    id_ligue=rl.getInt(1);
                }

                ResultSet rc=stmt2.executeQuery("select club_nom from club where id_ligue = "+id_ligue);
                while(rc.next()){
                    model.addElement(rc.getString(1));
                }

            }catch(Exception e){
                e.printStackTrace();
            }
        return model;
    }
    
    public String[] getDetailsClub(String club){
        this.getConnection();
        String req = "SELECT c.club_nom, c.club_activite, c.nb_Membres, l.ligue_nom, CONCAT(representant_nom, ' ', representant_prenom) AS affichage_nom "
                + "FROM club c, ligue l, representant r "
                + "WHERE c.club_nom = '"+club+"' AND c.id_ligue = l.id_ligue AND c.id_representant = r.id_representant";
        String[] array= new String[5];
        try{
           Statement stmt1=co.createStatement();
           ResultSet rs=stmt1.executeQuery(req);
           while (rs.next()){
               for (int i=1;i<=5;i++){
                    array[i-1]=rs.getString(i);
                }     
           } 
        }catch(Exception e){
            e.printStackTrace();
        }
        return array;
    }
    
    public void Formulaire_ligue(String nom_ligue, String activite, String nom, String prenom, String email){
        this.getConnection();
        try{
            Statement stmt1=co.createStatement();
            int rs =stmt1.executeUpdate("INSERT INTO representant(representant_nom,representant_mail,representant_prenom) VALUES('"+nom+"','"+email+"','"+prenom+"')");
            int id=0;
            ResultSet rl=stmt1.executeQuery("select MAX(id_representant) from representant");
            while (rl.next()){
                id=rl.getInt(1);
            } 
            int rc = stmt1.executeUpdate("INSERT INTO ligue(ligue_nom,activite,id_representant) VALUES('"+nom_ligue+"','"+activite+"',"+id+")");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void Formulaire_club(String club, String activite,String nb, String nom, String prenom, String email,String ligue){
        this.getConnection();
        try{
            Statement stmt1=co.createStatement();
            int rs =stmt1.executeUpdate("INSERT INTO representant(representant_nom,representant_mail,representant_prenom) VALUES('"+nom+"','"+email+"','"+prenom+"')");
            int id=0;
            int id_ligue=0;
            ResultSet rl=stmt1.executeQuery("select MAX(id_representant) from representant");
            while (rl.next()){
                id=rl.getInt(1);
            }
            ResultSet rm=stmt1.executeQuery("select id_ligue from ligue where ligue_nom='"+ligue+"'");
            while(rm.next()){
                id_ligue=rm.getInt(1);
            }
            int nombre = Integer.parseInt(nb);
            int rc = stmt1.executeUpdate("INSERT INTO club(club_nom,club_activite,nb_Membres,id_ligue,id_representant) VALUES('"+club+"','"+activite+"',"+nombre+","+id_ligue+","+id+")");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    Statement createStatement() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
