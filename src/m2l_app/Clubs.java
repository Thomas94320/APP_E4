/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m2l_app;

/**
 *
 * @author johan
 */
public class Clubs {
    private int id_club;
    private String club_nom;
    private String club_activite;
    
    
    public Clubs(int id_club, String club_nom, String club_activite){
        this.id_club= id_club;
        this.club_nom=club_nom;
        this.club_activite=club_activite;
    }
    
    public int getId(){
        return id_club;
    }
    
    public String getNom(){
        return club_nom;
    }
    
    public String getActivite(){
        return club_activite;
    }
}
